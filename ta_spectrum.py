import numpy as np
from scipy import constants
import scipy.fftpack as ft
from scipy.interpolate import interp1d
import yaml
import os
import warnings
from initialise_sims import InitialiseSims

k_B = constants.Boltzmann
q = constants.electron_volt
h = constants.h
hbar = constants.hbar
c = constants.speed_of_light
pi = constants.pi
m_e = constants.electron_mass


def energies_from_photon_energy(
    photon_energy, bandgap, mass_elec, mass_hole, e_cbm, e_vbm
):
    """
    Assuming spherical, parabolic bands, return the electron energy and hole energy that would be expected given a photon energy and a bandgap, following the energy conservation rule that E_photon = E_bandgap + E_electron + E_hole and the momentum conservation rule that k_electron = k_hole.
    """
    elec_energy = e_cbm + (photon_energy - bandgap) / (1 + mass_elec / mass_hole)
    hole_energy = e_vbm - (photon_energy - bandgap) / (1 + mass_hole / mass_elec)
    return elec_energy, hole_energy


def fermi_dirac(energies, temperature, fermi_level=0):
    """
    The Fermi-Dirac distribution.
    """
    fd = 1 / (np.exp((energies - fermi_level) / (k_B * temperature)) + 1)
    return fd


class TransientAbsorptionSpectrum:
    def __init__(self, spectrum_inputs_file):
        self.init_sims(spectrum_inputs_file)
        self.load_inputs(spectrum_inputs_file)
        self.set_probe_photon_energies()
    
    def init_sims(self, filename):
        """
        Create a class instance of 'InitialiseSims' to return the filepaths of the calculated electron and hole distributions.
        """
        calc_dist = InitialiseSims(filename)
        
        elec_dist_file_path = calc_dist.calc_elec_dist()  
        self.elec_dist_file = elec_dist_file_path 
         
        hole_dist_file_path = calc_dist.calc_hole_dist()  
        self.hole_dist_file = hole_dist_file_path 

    def load_inputs(self, filename):
        """
        Load the inputs required for the TA calculation from a .yaml file.
        """
        if os.path.exists(filename):
            with open(filename) as f:
                input_dict = yaml.full_load(f)

        for name, value in input_dict.items():
            setattr(self, name, value)
        self.bandgap = self.bandgap_ev * q
        self.mass_elec = self.mass_elec * m_e
        self.mass_hole = self.mass_hole * m_e

    def set_probe_photon_energies(self):
        """
        Generate an array of photon energies for the probe laser. Two out of three of: maximum probe energy, probe energy step and number of probe energy values must be specified in inputs.
        """
        self.photon_energies = q * self.get_arange_from_inputs(
            "probe_energy_max", "probe_energy_step", "probe_energy_n"
        )

    def get_arange_from_inputs(self, max_inp, step_inp, num_inp):
        """
        Return a numpy arange array, given strings that represent the inputs relating to the maximum value, the step value and the number of steps. A heirarchy of inputs is used in case all three are specified, although a warning is given in this case.

        If one of the three inputs is missing, or if all three are specified, the missing/extra attribute will be determined self-consistently using the other two.
        """
        if (
            hasattr(self, max_inp)
            and hasattr(self, step_inp)
            and hasattr(self, num_inp)
        ):
            warnings.warn(
                "Array has been overspecified (all three of {max_inp}, {step_inp} and {num_inp} are in input file). Array will be generated using {max_inp} and {step_inp}, and {num_inp} will be overwritten for consistency. This may not be the desired effect.".format(
                    max_inp=max_inp, step_inp=step_inp, num_inp=num_inp
                )
            )

        # Get variables if they exist
        if hasattr(self, max_inp):
            max_val = getattr(self, max_inp)
        if hasattr(self, num_inp):
            num_val = getattr(self, num_inp)
        if hasattr(self, step_inp):
            step_val = getattr(self, step_inp)

        # Fill in the rest if the other two have been specified
        if hasattr(self, max_inp) and hasattr(self, step_inp):
            num_val = int(max_val / step_val)
            setattr(self, num_inp, num_val)
        elif hasattr(self, max_inp) and hasattr(self, num_inp):
            step_val = max_val / num_val
            setattr(self, step_inp, step_val)
        elif hasattr(self, num_inp) and hasattr(self, step_inp):
            max_val = step_val * num_val
            setattr(self, max_inp, max_val)
        else:
            raise RuntimeError(
                "Two of three of {max_inp}, {step_inp} and {num_inp} must be specified to generate the desired array.".format(
                    max_inp=max_inp, step_inp=step_inp, num_inp=num_inp
                )
            )

        return np.arange(0, max_val, step_val)

    def calculate_transmission(self):
        """
        Calculate the transmission, plus all the other component parts.
        """
        self.perov_refr_index = self.get_perovskite_refractive_index(
            self.photon_energies, self.refr_file
        )
        if hasattr(self, "kappa_file"):
            self.perov_ext_coeff = self.get_perovskite_extinction_coefficient(
                self.photon_energies, self.kappa_file
            )
        self.alpha_0 = self.get_background_absorption(
            self.photon_energies, self.absorption_file
        )
        self.delta_absorption = self.get_delta_absorption(self.photon_energies)
        self.delta_refr_index = self.get_delta_refractive_index(self.delta_absorption)

        self.transmission = self.price_simple_transmission()

    def save_transmission_to_file(self, transmission_file):
        self.calculate_transmission()
        np.savetxt(
            transmission_file,
            np.column_stack([self.photon_energies / q, self.transmission]),
            header="Photon energy (eV)\tDifferential transmission (dimensionless)",
        )

    def transmission_values(self):
        """
        Return the simulated transmission values.
        """
        self.calculate_transmission()
        
        vals = np.column_stack([self.photon_energies / q, self.transmission])
        vals = vals[np.isfinite(vals[:, 1])]

        return vals
    
    def get_perovskite_refractive_index(self, energies, refr_file):
        """
        Get the perovskite refractive index from a file and interpolate it along the photon_energy axis.
        """
        refractive_index = self.load_pad_interpolate_energy_data(
            refr_file, energies / q
        )
        return refractive_index

    def get_perovskite_extinction_coefficient(self, energies, kappa_file):
        """
        Get the perovskite extinction coefficient from a file and interpolate it along the photon_energy axis.
        """
        extinction_coefficient = self.load_pad_interpolate_energy_data(
            kappa_file, energies / q
        )
        return extinction_coefficient

    def get_background_absorption(self, energies, absorption_file):
        """
        Get the background absorption coefficient from a file and interpolate it along the photon_energy axis.
        """
        absorption = 1e2 * self.load_pad_interpolate_energy_data(
            absorption_file, energies / q
        )
        return absorption

    def get_sim_distribution_function(
        self, dist_file, time_index, mass, carrier_density
    ):
        """
        Numerically calculate the effective distribution function from discrete carrier distributions recorded in BoltMC simulations.
        """
        data = np.load(dist_file)
        energies = data["distribution_energies"][:-1]
        bin_width = energies[1] - energies[0]
        carrier_distribution = data["distribution"][time_index]
        norm_carrier_distribution = (
            carrier_distribution / np.sum(carrier_distribution) / bin_width
        )
        dos = (
            1
            / (2 * pi**2)
            * (2 * mass / (hbar**2)) ** (3 / 2)
            * energies ** (1 / 2)
        )
        occupation_function = norm_carrier_distribution * carrier_density / dos
        return occupation_function

    def find_energy_index(self, energy, energy_distribution_file):
        """
        Find the index of a given energy within the array specifying the energy distribution bin edges used in recording from the BoltMC simulation.
        """
        carrier_energies = np.load(energy_distribution_file)["distribution_energies"]
        idx = np.abs(carrier_energies - energy).argmin()
        return idx

    def get_delta_absorption(self, energies):
        """
        Get \Delta \alpha by taking the difference of the absorption due to the electronic distributions and the background absorption.
        """
        continuum_alpha = self.alpha_0
        if self.calc_mode == "fd":
            distribution_alpha = self.get_fd_distribution_alpha(energies)
        elif self.calc_mode == "simulation":
            distribution_alpha = self.get_sim_distribution_alpha(energies)
        else:
            raise RuntimeError("{}.calc_mode must be 'fd' or 'simulation'".format(self))
        delta_alpha = distribution_alpha - continuum_alpha
        return -delta_alpha

    def get_delta_refractive_index(self, delta_absorption):
        """
        Get the change in refractive index caused by the change in absorption. See SI Equation 2 of Price et al. 2015.
        """
        extinct_coeff = hbar * c / (2 * self.photon_energies) * delta_absorption
        if self.photon_energies[0] == 0:
            extinct_coeff[0] = extinct_coeff[1]

        delta_refr_index = ft.hilbert(extinct_coeff)
        return delta_refr_index

    def price_simple_transmission(self):
        """
        The differential transmission as a function of the change in absorption coefficient and change in refractive index (relative to the unexcited sample). See eq. 3 in Price et al 2015.
        """
        self.lalpha = self.sample_thickness * self.delta_absorption
        n_g = self.glass_refr_index
        n_p = self.perov_refr_index
        self.nfrac = -(
            2
            * (n_g - n_p**2)
            / ((n_p**2 + n_p) * (n_g + n_p))
            * self.delta_refr_index
        )
        transmission = self.lalpha + self.nfrac
        return transmission

    def get_sim_distribution_alpha(self, energies):
        """
        Calculate the absorption of the semiconductor sample simulated in BoltMC given the simulated conduction and valence band carrier distributions.
        """
        self.cbm = self.bandgap
        self.vbm = 0

        back_abs = self.alpha_0

        con_energies, val_energies = energies_from_photon_energy(
            energies, self.bandgap, self.mass_elec, self.mass_hole, self.cbm, self.vbm
        )

        con_energies_prime = con_energies - self.cbm
        val_energies_prime = self.vbm - val_energies

        self.f_conduction = np.zeros(len(energies))
        self.f_valence = np.zeros(len(energies))

        elec_mean_energy = self.calc_mean_energy(self.elec_dist_file, self.time_idx)
        approx_elec_temp = elec_mean_energy / k_B
        hole_mean_energy = self.calc_mean_energy(self.hole_dist_file, self.time_idx)
        approx_hole_temp = hole_mean_energy / k_B

        for con_idx, elec_energy in enumerate(con_energies_prime):
            if elec_energy < 0:
                # for energies below bandgap, approximate distribution function as F-D
                elec_fermi_level = self.cbm + self.calculate_fermi_level(self.mass_elec)
                self.f_conduction[con_idx] = fermi_dirac(
                    elec_energy + self.cbm,
                    approx_elec_temp,
                    fermi_level=elec_fermi_level,
                )
            else:
                sim_idx = self.find_energy_index(elec_energy, self.elec_dist_file)
                self.f_conduction[con_idx] = self.get_sim_distribution_function(
                    self.elec_dist_file,
                    self.time_idx,
                    self.mass_elec,
                    self.carrier_density,
                )[sim_idx]

        for val_idx, hole_energy in enumerate(val_energies_prime):
            if hole_energy < 0:
                # for energies below bandgap, approximate distribution function as F-D
                hole_fermi_level = self.vbm - self.calculate_fermi_level(self.mass_hole)
                self.f_valence[val_idx] = fermi_dirac(
                    self.vbm - hole_energy,
                    approx_hole_temp,
                    fermi_level=hole_fermi_level,
                )
            else:
                sim_idx = self.find_energy_index(hole_energy, self.hole_dist_file)
                self.f_valence[val_idx] = (
                    1
                    - self.get_sim_distribution_function(
                        self.hole_dist_file,
                        self.time_idx,
                        self.mass_hole,
                        self.carrier_density,
                    )[sim_idx]
                )

        alpha = back_abs * (self.f_valence - self.f_conduction)

        return alpha

    def get_fd_distribution_alpha(self, energies):
        """
        Get the absorption due to the background absorption and the absorption due to the charge carrier distributions.
        """
        self.cbm = self.bandgap
        self.vbm = 0

        back_abs = self.alpha_0

        con_energies, val_energies = energies_from_photon_energy(
            energies, self.bandgap, self.mass_elec, self.mass_hole, self.cbm, self.vbm
        )
        self.elec_fermi_level = self.cbm + self.calculate_fermi_level(self.mass_elec)
        self.hole_fermi_level = self.vbm - self.calculate_fermi_level(self.mass_hole)
        self.f_conduction = fermi_dirac(
            con_energies, self.temperature, fermi_level=self.elec_fermi_level
        )
        self.f_valence = fermi_dirac(
            val_energies, self.temperature, fermi_level=self.hole_fermi_level
        )
        alpha = back_abs * (self.f_valence - self.f_conduction)

        return alpha

    def calc_mean_energy(self, dist_file, time_idx):
        "Calculate the mean carrier energy from the carrier distribution."
        data = np.load(dist_file)
        bin_edges = data["distribution_energies"]
        distribution = data["distribution"][time_idx]
        total_particles = np.sum(distribution)
        sum = 0
        for idx, value in enumerate(distribution):
            sum = sum + value * bin_edges[idx]
        return sum / total_particles

    def calculate_fermi_level(self, mass):
        """
        Calculate the Fermi level using the effective density of states, see e.g. https://www.tf.uni-kiel.de/matwis/amat/semi_en/kap_2/backbone/r2_3_2.pdf
        """
        effective_dos = 2 * (2 * pi * mass * k_B * self.temperature / h**2) ** 1.5
        fermi_level = (
            k_B * self.temperature * np.log(self.carrier_density / effective_dos)
        )
        return fermi_level

    # def calculate_fermi_level(self, mass):
    #     """
    #     Calculate the Fermi level using the effective density of states, see Nilsson APL 1978, eq. 4
    #     """
    #     effective_dos = 2 * (2 * pi * mass * k_B * self.temperature / h**2) ** 1.5
    #     u = self.carrier_density / effective_dos
    #     kT = k_B * self.temperature
    #     fermi_level = kT * (
    #         np.log(u) + u / ((64 + 0.05524 * u * (64 + u**0.5)) ** 0.25)
    #     )
    #     return fermi_level

    def load_pad_interpolate_energy_data(
        self, filename, interpolation_energies, pad_value=0
    ):
        """
        Wrapper for load_data and interpolate_data, plus intermediate step of padding arrays.
        """
        x, y = self.load_data(filename)
        pad_ids = interpolation_energies < min(x)
        padded_x = np.append(interpolation_energies[pad_ids], x)
        padded_y = np.append(
            np.full(len(interpolation_energies[pad_ids]), pad_value), y
        )
        smoothed_y = self.interpolate_data(padded_x, padded_y, interpolation_energies)
        return smoothed_y

    def load_data(self, filename):
        """
        Load a csv file containing an x and y variable in two columns and return x and y arrays.
        """
        raw_data = np.loadtxt(filename, skiprows=1, delimiter=",", usecols=range(2))
        x = raw_data[:, 0]
        y = raw_data[:, 1]
        return x, y

    def interpolate_data(self, absorption_energies, absorption_points, fit_energies):
        """
        Smooth some absorption data using scipy's 1d interpolator and return interpolated absorption data along the axis of fit_energies.
        """
        f1 = interp1d(absorption_energies, absorption_points)
        return f1(fit_energies)
