from scipy import constants
import yaml
from boltmc.simulation import Simulation 

h = constants.Planck
c = constants.speed_of_light
q = constants.elementary_charge

class InitialiseSims:
    # Take yaml file as input and make parameters attributes of class
    def __init__(self, filename):
        self.filename = filename
        self.load_inputs()

        self.m_e = self.params['mass_elec']
        self.m_h = self.params['mass_hole']
        self.bandgap = self.params['bandgap_ev']
        self.cbm = self.bandgap
        self.vbm = 0.0
        self.pump_mean_energy = self.params['pump_mean_energy']
        self.pump_stdev_energy = self.params['pump_stdev_energy']
        self.mass_fact = 1 / (1 + self.m_e / self.m_h)
    
    # Function to load inputs from yaml file
    def load_inputs(self):
        with open(self.filename, 'r') as f:
            self.params = yaml.safe_load(f)

    # Conduction electron mean energy
    def con_mean_energy(self):
        return self.mass_fact * (self.pump_mean_energy + (self.m_e / self.m_h) * self.cbm - self.bandgap)
    # Standard deviation in conduction electron energy
    def con_stdev_energy(self):
        return self.mass_fact * self.pump_stdev_energy
    # Valence hole mean energy
    def val_mean_energy(self):
        return -1*self.mass_fact * (self.pump_mean_energy + (self.m_e / self.m_h) * self.vbm - self.bandgap)
    # Standard deviation in valence hole energy
    def val_stdev_energy(self):
        return self.mass_fact * self.pump_stdev_energy
    # Update material parameter file
    def update_params(self):
        input_file = 'files/mapbi3_05.yaml'
        with open(input_file, 'r') as f:
            params = yaml.safe_load(f)
        # Insert new effective masses - could extend to dielectric constants
        params['mstar'][0][0] = self.m_e  
        params['mstar'][1][0] = self.m_h  

        with open(input_file, 'w') as f:
            yaml.dump(params, f)

        return input_file

    # Calculate carrier energy distributions - band type: 'con' or 'val', particle type: 'elec' or 'hole'
    def calc_distribution(self, band_type, particle_type):
        # Update material parameters
        self.update_params()
        # Calculate mean and standard deviation in energy
        mean_energy = getattr(self, f'{band_type}_mean_energy')()
        stdev_energy = getattr(self, f'{band_type}_stdev_energy')()
        # Specify BoltMC parameters
        dist_params = {
            "initial_gaussian_centre": mean_energy, 
            "initial_gaussian_width": stdev_energy,
            "distribution_energy_max": mean_energy*3,
            "distribution_energy_step": (mean_energy*3)/1000,
            "screenlen_max_temp": 18000,
            "rate_table_energy_max": 2.0,
            "n_particles": 5000,
            "matfilepath": '/home/PROJECTS/2023-24/cfvn20/boltmc_space/mcmc_spectra/files',
            "matfilenames": ['mapbi3_05.yaml'],
            "custom_extensions": {'RecordEnergyDistributions': {'args': {'outfile': f'data/dist_energies_{particle_type}.npz'}},
                              'SaveSimulation': {'args': {'outfile': f'data/{particle_type}_dist.json'}}}
        }

        # Run BoltMC and return energy distribution
        sim = Simulation(infile=f'files/{particle_type}_sim.yaml', params=dist_params)
        sim.run()
        return f'data/dist_energies_{particle_type}.npz'

    # Calculate electron energy distribution
    def calc_elec_dist(self):
        return self.calc_distribution('con', 'elec')
    # Calculate hole energy distribution
    def calc_hole_dist(self):
        return self.calc_distribution('val', 'hole')


        

