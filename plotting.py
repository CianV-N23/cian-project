import matplotlib.pyplot as plt
import pandas as pd 
import numpy as np

# Experimental data
exp_file = 'data/transmission_exp.dat'
y_exp = np.loadtxt(exp_file)
y_exp = y_exp[np.isfinite(y_exp[:, 1])]

# Simulated data
sim_file = 'data/transmission_sim.dat'
y_sim = np.loadtxt(sim_file)
y_sim = y_sim[np.isfinite(y_sim[:, 1])]

# Posterior samples
data = pd.read_csv('data/samples.csv', header=None)

# Plot traces for each parameter
plt.figure()
plt.plot(np.exp(data[0]/10))
plt.xlabel('Iteration')
plt.ylabel('elec_mass')
plt.savefig('figures/samples_elec.png')

plt.figure()
plt.plot(np.exp(data[1]/10))
plt.xlabel('Iteration')
plt.ylabel('hole_mass')
plt.savefig('figures/samples_hole.png')


plt.figure()
plt.plot(np.exp(data[2]))
plt.xlabel('Iteration')
plt.ylabel('sample_thickness')
plt.savefig('figures/samples_thickness.png')



