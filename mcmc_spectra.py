import numpy as np
import yaml
from ta_spectrum import TransientAbsorptionSpectrum

class Posteriors:
    # Take experimental data 'y_exp' and initial guesses for each parameter 'theta' as input
    def __init__(self, theta, y_exp):
        self.y_exp = y_exp
        self.theta = theta

    # Define function to run BoltMC and spectra code
    def run_sim(self, theta):
        input_file = 'files/inputs_sim.yaml'
        with open(input_file, 'r') as f:
            params = yaml.safe_load(f)

        # Input parameters scaled to recover original values
        params['mass_elec'] = np.float(np.exp(theta[0]/10))
        params['mass_hole'] = np.float(np.exp(theta[1]/10))
        params['sample_thickness'] = np.float(np.exp(theta[2]))
        # params['carrier_density'] = np.float(np.exp(theta[3])) - could extend to carrier density

        with open(input_file, 'w') as f:
            yaml.dump(params, f)

        # Calculate simulated transmission values 'y_sim'
        sim = TransientAbsorptionSpectrum(input_file)
        y_sim = sim.transmission_values()
        return y_sim
    
    # Define log-likelihood function using run_sim
    def log_likelihood(self, theta):
        # Transmission values on the order of 10e-03 so chose 10e-04 for uncertainty
        sigma_theta = 0.0001 
        y_sim = self.run_sim(theta)
        # Gaussian likelihood - returns higher probability for smaller difference between 'y_sim' and 'y_exp'
        log_likelihood = -0.5 * np.sum(((y_sim - self.y_exp) / sigma_theta)**2) 
        return log_likelihood
    
    # Define log-prior distribution as a multivariate Gaussian distribution defined by prior means and standard deviations
    def log_prior(self, theta):
        # Prior means: 0.15 m_o, 0.12 m_o (Brivio F et al. 2014. Physical review B, 89, 15.) and 80 nm (McCallum et al. 2023. Journal of Physics: Energy, 6, 015005.) 
        # Natural log of each parameter taken with mass_elec and mass_hole multiplied by 10
        prior_means = np.array([-18.9,-21.2,-16.34])  
        # Prior standard deviations: gives approximately +/- 0.1 m_o, 0.1 m_o and 50 nm for mass_elec, mass_ hole and sample_thickness
        sigma_thetas = [1,1,0.5] 
        # Gaussian prior - returns higher probability for smaller difference between 'theta' and 'prior_means'
        log_prior_pdfs = -0.5 * np.sum(((theta - prior_means) / sigma_thetas)**2)
        log_prior = np.sum(log_prior_pdfs)
        return log_prior
    
    # Define posterior by summing likelihood and prior - this will also be Gaussian
    def log_posterior(self, theta):
        return self.log_likelihood(theta) + self.log_prior(theta)

    # Define function to propose new state
    def propose(self, current_state):
        # Jump sizes for each parameter - gives average acceptance rate of approxiamtely 15%
        jump = np.array([0.007, 0.007, 0.001]) * np.abs(current_state)
        # Sample from normal distribution with standard deviation equal to jump size to determine new state
        return current_state + np.random.normal(0, jump, size=len(current_state))
    
    # Define Metropolis-Hastings algorithm 
    def MCMC_sampler(self, samples):
        # Determine current state and current log-posterior from input
        current_state = self.theta
        current_log_posterior = self.log_posterior(current_state)

        # Initialise posterior values and acceptance rate
        posterior = [current_state]
        acceptance_rate = 0.0

        # Repeat algorithm for selected number of samples
        for i in range(samples):
            # Propose new state and evaluate proposed log-posterior
            proposed_state = self.propose(current_state)
            proposed_log_posterior = self.log_posterior(proposed_state)
            # Calculate acceptance ratio
            acceptance_ratio = min(1, np.exp(proposed_log_posterior - current_log_posterior))
            # Accept or reject proposed state
            if np.random.uniform() < acceptance_ratio:
                current_state = proposed_state
                current_log_posterior = proposed_log_posterior
                acceptance_rate += 1.0
            # Add current state to posterior samples
            posterior.append(current_state)

        # Calculate acceptance rate and return all posterior samples
        acceptance_rate = acceptance_rate / samples
        return np.array(posterior), acceptance_rate

    # Function to set off Markov chain for a selected number of samples             
    def run_MCMC(self, samples):
        samples, acceptance_rate = self.MCMC_sampler(samples=samples)
        return samples, acceptance_rate


if __name__ == "__main__":

    # File for experimental data  
    exp_file = 'data/transmission_exp.dat'
    y_exp = np.loadtxt(exp_file)
    y_exp = y_exp[np.isfinite(y_exp[:, 1])]
    # Initital guesses for each parameter: 0.17 m_o, 0.14_mo and 80 nm
    theta = [10*np.log(0.17), 10*np.log(0.14), np.log(80.0e-09)]
    # Create class instance, select number of samples and set off chain
    find_posteriors = Posteriors(theta, y_exp)
    samples = 1000
    samples, acceptance_rate = find_posteriors.run_MCMC(samples)
    # Save samples and acceptance rate
    np.savetxt('data/samples.csv', samples, delimiter=',')
    np.savetxt('data/acceptance.csv', np.array([acceptance_rate]), delimiter=',')



